alias ..='cd ../'
alias ...='cd ../../'
alias jbos='jobs'
alias sl='ls'
alias gittree="git log --graph --all --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'"
