source ~/.profile
export BASH_SILENCE_DEPRECATION_WARNING=1

export PATH="$HOME/.poetry/bin:$PATH"
. "$HOME/.cargo/env"
