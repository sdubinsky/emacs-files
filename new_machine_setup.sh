#!/bin/sh
set -e
if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    sudo /usr/lib/apt/apt-helper download-file https://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2020.02.03_all.deb keyring.deb SHA256:c5dd35231930e3c8d6a9d9539c846023fe1a08e4b073ef0d2833acd815d80d48
    sudo dpkg -i ./keyring.deb
    echo "deb https://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" | sudo tee /etc/apt/sources.list.d/sur5r-i3.list

    sudo dnf update
    sudo dnf -y install curl apcalc screen ghc git sway waybar cowsay fortune-mod postgresql postgresql-devel cowsay adb net-tools python syncthing bemenu ripgrep emacs sqlite fzf libtool-bin ledger
    #This is for light, the program to change the backlight
    wget -O "hackttf.zip" https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip
    unzip "hackttf.zip"
    sudo cp -r "ttf" /usr/local/share/fonts
    sudo rm /usr/bin/emacs
    sudo ln -s $(which emacs-28.2) /usr/bin/emacs
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0

    . ~/.asdf/asdf.sh
    . ~/.asdf/completions/asdf.bash
    asdf plugin add ruby

elif [ "$(uname -s)" == "Darwin" ]; then
    xcode-select --install

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew tap dimentium/autoraise
    brew tap jimeh/emacs-builds
    brew install ffmpeg android-platform-tools cowsay fortune gcc ledger make markdown mongodb pandoc postgresql python wget youtube-dl syncthing rename curl tmux mosh git cowsay fortune rename ripgrep sqlite3 apcalc coreutils asdf fzf autoraise
    brew install --cask emacs-app
    brew services start dimentium/autioraise/autoraise
    . $(brew --prefix asdf)/asdf.sh
    . $(brew --prefix asdf)/etc/bash_completion.d/asdf.bash

    $(brew --prefix)/opt/fzf/install

    mkdir -p ~/.config/kitty/
    ln -s ~/emacs-files/kitty.conf  ~/.config/kitty/kitty.conf
fi

git config --global user.name "Shalom Dubinsky"
git config --global user.email "smdubinsky@gmail.com"

cd ~
git clone https://github.com/sdubinsky/emacs-files.git
rm -f .bashrc .bash_profile .bash_aliases .profile
ln -s ~/emacs-files/.bashrc ~/.bashrc
ln -s ~/emacs-files/.bash_profile ~/.bash_profile
ln -s ~/emacs-files/.profile ~/.profile
ln -s ~/emacs-files/.bash_aliases ~/.bash_aliases
touch emacs-files/locals.el
mkdir -p ~/.emacs.d
if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    ln -s ~/emacs-files/init.el ~/.emacs.d/init.el
    ln -s ~/emacs-files/emacs.service ~/.config/systemd/user/emacs.service
    ln -s ~/emacs-files/urserver.service ~/.config/systemd/user/urserver.service
fi

cd ~
mkdir code

echo Things to do after:
echo "1: set natural scrolling(libinput): https://askubuntu.com/questions/1122513/how-to-add-natural-inverted-mouse-scrolling-in-i3-window-manager"
echo "to check which driver, run xinput to find the id of the mouse, then xinput list-props mouse_id to see which driver is listed."
echo "run `emacs` to download all the packages, then run `systemctl --user enable emacs && systemctl --user start emacs` to start the server"
echo "and do the same thing for urserver"
echo "Install sway and wlroots with custom patch from source"
