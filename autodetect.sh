set -e

count=`xrandr --query | grep "\bconnected\b" | wc -l`

if [ $count = 3 ]
then
    xrandr --output DP-0 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI-0 --off --output eDP-1-1 --primary --mode 1920x1080 --pos 3840x0 --rotate normal
elif [ $count = 2 ]
then
    xrandr --output DP-0 --off --output DP-1 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-0 --off --output eDP-1-1 --primary --mode 1920x1080 --pos 1920x158 --rotate normal
else
    xrandr --output eDP-1-1 --primary
fi
set +e
