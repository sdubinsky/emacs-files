;;disable modes
(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(setq-default indent-tabs-mode nil)
(add-hook 'minibuffer-setup-hook 'visual-line-mode)
;; from https://zck.org/improved-emacs-search
(setq isearch-lazy-count t)
(setq lazy-count-prefix-format nil)
(setq lazy-count-suffix-format " (%s/%s)")
;;Set other modes
(global-auto-revert-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)
;; disable bell function
(setq ring-bell-function 'ignore)
;;This lets me use the ergodox on the mac the same way I would on linux.
;;Part 1 of the setup here involves going into mac system preferences and swapping
;;ctrl and cmd for the external keyboard.
;;Then change the spotlight command from cmd-space to ctrl-space
(setq mac-command-modifier 'control)
(setq mac-control-modifier 'meta)
(setq initial-scratch-message "")
(setq tab-always-indent 'complete)

;; minibuffer height
(setq max-mini-window-height 0.10)

;;force to split horizontally
;;https://www.emacswiki.org/emacs/HorizontalSplitting
(setq split-height-threshold 400)
(setq split-width-threshold 80)

;;Custom elisp functions
;;show the matching parenthesis
(show-paren-mode 1)
(setq show-paren-delay 0)
(setq show-paren-style 'expression);;highlight whole expression
;;Show offscreen parens in minibuffer
(defadvice show-paren-function
    (after show-matching-paren-offscreen activate)
  "If the matching paren is offscreen, show the matching line in the
        echo area. Has no effect if the character before point is not of
        the syntax class ')'."
  (interactive)
  (let* ((cb (char-before (point)))
         (matching-text (and cb
                             (char-equal (char-syntax cb) ?\) )
                             (blink-matching-open))))
    (when matching-text (message matching-text))))

;;fix for ledger mode colors.  Colors taken from zenburn-theme
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-blue ((t (:foreground "#8CD0D3"))))
 '(ansi-color-red ((t (:foreground "#CC9393"))))
 '(web-mode-block-face ((t (:background nil))))
 '(web-mode-symbol-face ((t (:foreground "#8CD0D3")))))
;;move windows with shift-arrow
;;https://stackoverflow.com/questions/16607791/emacs-move-around-split-windows-in-a-specified-direction
;;disabled until I figure out how to get it to play with org-mode
;;(windmove-default-keybindings)

;;scratch buffer default mode - https://emacs.stackexchange.com/questions/53875/change-scratch-buffer-to-org-mode-by-default
(setq initial-major-mode 'org-mode)
;;sort dired by numbers, not sure what all the switches do
(setq dired-listing-switches "-1aGh1vl")
(prefer-coding-system 'utf-8)
(put 'narrow-to-region 'disabled nil)
;; set large file warning threshold higher
(setq large-file-warning-threshold 900000000)

;;native scrolling - https://emacs.stackexchange.com/questions/26988/set-scroll-amount-for-mouse-wheel-in-emacs
;;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))

;;I don't really want to accidentally suspend emacs a lot
;;If this comes up a lot in terminal, redefine the key instead
;;from https://www.gnu.org/software/emacs/manual/html_node/emacs/Disabling.html#Disabling
(global-set-key (kbd "C-z") nil)
;;this is just obnoxious
(global-set-key (kbd "C-v") nil)
(global-set-key (kbd "M-v") nil)

(global-set-key (kbd "C-x C-b") 'switch-to-buffer)
(global-set-key (kbd "<mouse-9>") 'set-mark-command)
(global-set-key (kbd "<mouse-8>") 'exchange-point-and-mark)

;;conservative scrolling

;;use cmd as meta as well
(setq mac-command-key-is-meta t)

;;Spellchecking in latex mode
(add-hook 'latex-mode-hook 'flyspell-mode)

;;speed up tramp
(setq tramp-default-method "ssh")

;;set font to Hack, if it exists
(add-to-list 'default-frame-alist '(font . "Hack-16"))
(setq my-ledger-file "~/Documents/finances/ledger/ledger-2025.dat")
(load "~/emacs-files/locals.el")

;;Load autoinstalled packages(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))
(package-initialize)

;; disable splash screen
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doc-view-continuous t)
 '(face-font-family-alternatives
   '(("Monospace" "courier" "fixed")
     ("courier" "CMU Typewriter Text" "fixed")
     ("Sans Serif" "helv" "helvetica" "arial" "fixed")
     ("helv" "helvetica" "arial" "fixed")))
 '(inhibit-startup-screen t)
 '(org-agenda-files '("~/code/xoala/todo.org"))
 '(org-capture-templates
   '(("t" "todo list" entry (file "~/code/xoala/todo.org")
      "* TODO (%(org-read-date)) WLC-%^{ticket id}: %^{summary}\12** Overview\12***\12** [/] Questions\12- [ ]\12** Notes\12***\12** [/] Plan\12- [ ]\12** [/] Tests\12- [ ]"
      :prepend t :immediate-finish t)
     ("z" "razroo todo list" entry (file "~/code/razroo/todo.org")
      "* TODO (%(org-read-date)) ZETA-%^{ticket id}: %^{summary}\12** Overview\12***\12** [/] Questions\12- [ ]\12** Notes\12***\12** [/] Plan\12- [ ]\12** [/] Tests\12- [ ]"
      :prepend t :immediate-finish t)
     ("n" "Take a note" item (file "~/org/notes.org")
      "%(org-read-date): %^{note}" :prepend t :immediate-finish t)
     ("l" "insert item into ledger" plain (file my-ledger-file)
      "%(org-read-date) %^{Payee}\12\11%^{Account|Expenses:Groceries|Expenses:Eating Out|Expenses:Transportation:Public Transit|Expenses:Transportation:Repairs|Expenses:Transportation:Fees|Expenses:Transportation|Expenses:Electronica|Expenses:Apartment:Household Needs|Expenses:Apartment:Internet|Expenses:Apartment:Electricity|Expenses:Apartment:Water|Expenses:Apartment:Arnona|Expenses:Apartment:Vaad Bayit|Expenses:Basic Necessities|Expenses:Medical|Expenses:Entertainment|Expenses:Fencing Database:Hosting|Expenses:Title Reader:Hosting|Expenses:Fencing:Membership|Expenses:Fencing:Lessons|Expenses:Fencing:Equipment|Expenses:Fencing:Tournaments|Expenses:Dating|Expenses:Gifts|Expenses:Phone|Expenses:Business Expenses|Accounts:Bankroll|Accounts:Poker|Accounts:Credit Cards:BHP Credit|Accounts:Credit Cards:Discover|Accounts:Credit Cards:Paybox Card|Assets:BHP Checking|Assets:Cash|Assets:Ally Checking|Expenses:Charity|Expenses:Vacation|Expenses:Clothing|Assets:Paybox Box}  %^{Currency|NIS |$|EUR }%^{Amount}\12\11%^{Payer|Accounts:Credit Cards:Paybox Card|Accounts:Credit Cards:BHP Credit|Assets:BHP Checking|Assets:Cash|Assets:Ally Checking|Accounts:Credit Cards:Discover|Accounts:Poker|Assets:Paybox Box|Income:Tutoring|Income:Salary|Income:Interest|Accounts:Bankroll}"
      :empty-lines 1)))
 '(package-selected-packages nil)
 '(pyvenv-mode nil)
 '(warning-suppress-log-types '((comp))))


;;Tab size
(setq-default tab-width 2)
(require 'package)

(eval-when-compile
	(require 'use-package))
(setq use-package-always-ensure t)
(setq use-package-always-defer t)

;;dark high-contrast theme
(use-package hc-zenburn-theme
  :init
  (load-theme 'hc-zenburn t))

;; doom modeline.  Don't forget to run (nerd-icons-install-fonts)
(use-package nerd-icons)
(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :config
  (setq doom-modeline-icon t)
  (setq doom-modeline-unicode-fallback nil)
  (setq doom-modeline-buffer-encoding nil))

;;manually-set variables
(global-set-key [insert] 'realgud:pry)
;;goto line
(global-set-key "\C-l" 'goto-line)
;;go one frame backward
(global-set-key (kbd "C-x x") '(lambda () "frame-back one" (interactive) (other-window -1)))
;; cycle through amounts of spacing - from http://pragmaticemacs.com/emacs/cycle-spacing/
(global-set-key (kbd "M-SPC") 'cycle-spacing)

;;calculate miles per gallon
(defun mpg (old new liters)
  "Calculate MPG."
  (interactive "nold mileage: \nnnew mileage: \nnliters: ")
  (insert (format
           ";;%.2f liters\n;;%.2f MPG.\n;;ODO: %d"
           liters (* (/ (- new old) liters) 2.352) new)))

(setq nxml-sexp-element-flag t)
(use-package diminish)

(use-package magit
  :bind
  ("C-x g" . magit-status))

;;forge + setup
(use-package forge
  :after magit)
(setq auth-sources '("~/.emacs.d/.authinfo"))

(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize)
  :defer 0)

(use-package sql
  :ensure nil
  :config
  (sql-set-product 'postgres))

;;advice
;;https://framagit.org/steckerhalter/steckemacs.el/blob/master/steckemacs.el
(use-package advice
  :ensure nil
  :config
  (defadvice kill-buffer (around kill-buffer-around-advice activate)
    "Bury scratch instead of killing it"
    (let ((buffer-to-kill (ad-get-arg 0)))
      (if (equal buffer-to-kill "*scratch*")
          (bury-buffer)
        ad-do-it))))

;;undo-tree-mode better undoing and redoing
(use-package undo-tree
  :bind (("C-c z" . undo-tree-visualize))
  :config
  (global-undo-tree-mode t))

;;realgud better debugging
(use-package realgud
  :defer 0
  :config
  (put 'realgud:pdb-command-name 'safe-local-variable #'stringp)
  (setq realgud:pdb-command-name "python"))

(use-package realgud-pry)

;;org-mode settings
(use-package org
	:config
	(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
	(add-hook 'org-mode-hook 'visual-line-mode)
  (global-font-lock-mode 1)
  (setq org-clock-persist 'history)
  (org-clock-persistence-insinuate)
  (setq org-startup-indented 1)
  (setq org-clock-idle-time 15)
  (setq org-return-follows-link 1)
  (setq org-directory "~/org notes and todos")
  (setq org-todo-keywords '((sequence "TODO(t)" "CURR(c)" "BLKD(b)" "|" "CR(r)" "DONE(d)" "WFIX(w)")))
  :diminish org-indent-mode
  :bind
	("C-c l" . org-store-link)
	("C-c a" . org-agenda)
  ("C-c C-x C-a" . nil)
  ("C-c c" . org-capture))

(use-package svg-tag-mode
  :config
  (setq svg-tag-tags
        '(("TODO" . ((lambda (tag) (svg-tag-make tag :face 'org-todo :font "Hack" :font-size 14))))
          ("CURR" . ((lambda (tag) (svg-tag-make tag :face 'org-todo :font "Hack" :font-size 14))))
          ("BLKD" . ((lambda (tag) (svg-tag-make tag :face 'org-todo :font "Hack" :font-size 14))))
          ("CR" . ((lambda (tag) (svg-tag-make tag :face 'org-done :font "Hack" :font-size 14))))
          ("DONE" . ((lambda (tag) (svg-tag-make tag :face 'org-done :font "Hack" :font-size 14))))
          ("WFIX" . ((lambda (tag) (svg-tag-make tag :face 'org-done :font "Hack" :font-size 14))))))
  :hook
  (org-mode . svg-tag-mode))

;; needed to make svg-tag-mode work with emacsclient
;;https://github.com/rougier/svg-lib/issues/18
(defun first-graphical-frame-hook-function ()
  (remove-hook 'focus-in-hook #'first-graphical-frame-hook-function)
  (provide 'my-gui))
(add-hook 'focus-in-hook #'first-graphical-frame-hook-function)
(with-eval-after-load 'my-gui
  (setq svg-lib-style-default (svg-lib-style-compute-default)))

;;visual line mode in text mode
(add-hook 'text-mode-hook 'visual-line-mode)

;;lua-mode
(use-package lua-mode
	:config
	(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
	(add-to-list 'interpreter-mode-alist '("lua" . lua-mode)))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;;Markdown mode
(use-package markdown-mode
	:config
	(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
	(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
	(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
  (setq markdown-command "pandoc -s"))

;;Yaml mode
(use-package yaml-mode)

;;Aggressive-indent-mode instead of electric-indent-mode.  Will indent blocks automatically.
(use-package aggressive-indent
	:config
	(global-aggressive-indent-mode 1)
	(add-to-list 'aggressive-indent-excluded-modes 'html-mode)
	(add-to-list 'aggressive-indent-excluded-modes 'ess-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'python-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'rust-mode)
	:diminish aggressive-indent-mode)

;;Haskell
(use-package haskell-tng-mode
  :ensure nil
  :load-path "/home/deus-ex/.emacs.d/haskell-tng.el/"
  :mode ((rx ".hs" eos) . haskell-tng-mode)

  :config
  (require 'haskell-tng-extra)
  (require 'haskell-tng-extra-abbrev)
  (require 'haskell-tng-extra-hideshow)
  (require 'haskell-tng-extra-projectile)
  (require 'haskell-tng-extra-yasnippet)
  ;; (require 'haskell-tng-extra-cabal-mode)
  (require 'haskell-tng-extra-stack)

  :bind
  (:map
   haskell-tng-mode-map
   ("RET" . haskell-tng-newline)
   ("C-c c" . haskell-tng-compile)
   ("C-c e" . next-error)))

;;Python
(use-package auto-virtualenv
  :defer 0
	:config
	(add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
  (add-hook 'projectile-after-switch-project-hook (lambda () (exec-path-from-shell-copy-env "PATH")))
	(add-hook 'projectile-after-switch-project-hook 'auto-virtualenv-set-virtualenv))

(use-package pyvenv
  :diminish t
  :hook (python-mode . pyvenv-mode))

(use-package poetry
  :config
  (poetry-tracking-mode t))

(use-package python-pytest
  :bind
  ("C-c , v" . python-pytest-file)
  ("C-c , s" . python-pytest-function)
  (:map python-pytest-mode-map
        ("g" . python-pytest-repeat)))

;;Anzu - show count of matches
(use-package anzu
	:config
	(global-anzu-mode 1)
	;;(global-set-key (kbd "M-%") 'anzu-query-replace)
	;;(global-set-key (kbd "C-M-%") 'anzu-query-replace-regexp)
	:diminish anzu-mode
	)

;;project management
(use-package projectile
  :defer 0
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :config
  (projectile-register-project-type 'sinatra '("Gemfile")
                                    :run "bundle exec ruby config.ru"
                                    :test "bundle exec rake test"
                                    :test-dir "test"
                                    :test-prefix "test_")
  (projectile-register-project-type 'mymy '("omnisharp.json")
                                    :compile "dotnet build"
                                    :run "dotnet run"
                                    :test "ASPNETCORE_ENVIRONMENT=test dotnet test -nologo")

	(projectile-mode)
  (put 'projectile-project-test-cmd 'safe-local-variable #'stringp)
  (setq projectile-switch-project-action #'projectile-commander)
  :diminish projectile-mode)

(use-package ripgrep
  :ensure-system-package (rg . ripgrep))

(use-package bundler)
;;next set of packages are for rails
(use-package projectile-rails
	:config
	(add-hook 'projectile-mode-hook 'projectile-rails-on)
	(define-key projectile-rails-command-map (kbd "A") (lambda() (interactive) (shell-command "bundle exec rake test")))
	)
(lambda() (shell-command "bundle exec rake test"))

(use-package rspec-mode
  :defer 0
  :config
  (setq rspec-use-opts-file-when-available nil)
  (setq rspec-command-options "")
	:diminish rspec-mode)

(use-package web-mode
  :defer 0
  :config
  (setq web-mode-enable-auto-closing t)
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-auto-expanding t)
  (setq web-mode-enable-auto-opening t)
  (setq web-mode-enable-auto-quoting t)
	:mode
	("\\.phtml\\'" . web-mode)
	("\\.tpl\\.php\\'" . web-mode)
	("\\.[agj]sp\\'" . web-mode)
	("\\.as[cp]x\\'" . web-mode)
	("\\.erb\\'" . web-mode)
	("\\.html?\\'" . web-mode)
	("\\.mustache\\'" . web-mode)
	("\\.djhtml\\'" . web-mode)
  ("\\.cshtml\\'" . web-mode))

(use-package json-mode
  :mode
  ("\.json$" . json-mode)
  :config
  (setq js-indent-level 2))

(use-package robe
	:config
	(add-hook 'ruby-mode-hook 'robe-mode)
	:diminish robe-mode)

(use-package feature-mode
  :mode
  ("\.feature$" . feature-mode))

;; .ini files
(use-package ini-mode
	:diminish ini-mode)

(use-package yasnippet
  :init
  (yas-global-mode 1)
  :config
  (add-to-list 'yas-snippet-dirs "~/emacs-files/snippets")
  :diminish yas-mode)

(use-package yasnippet-snippets)

;;Ruby
;;#{} automatic completion in strings - from http://blog.senny.ch/blog/2012/10/06/emacs-tidbits-for-ruby-developers/
(setq ruby-insert-encoding-magic-comment nil)
(defun senny-ruby-interpolate ()
  "In a double quoted string, interpolate."
  (interactive)
  (insert "#")
  (when (and
         (looking-back "\".*")
         (looking-at ".*\""))
    (insert "{}")
    (backward-char 1)))

(eval-after-load 'ruby-mode
	'(progn
		 (define-key ruby-mode-map (kbd "#") 'senny-ruby-interpolate)))

(add-to-list 'auto-mode-alist '("\\.god$" . ruby-mode))
(use-package inf-ruby
  :hook
  (compilation-filter . inf-ruby-auto-enter)
  :config
  (setq inf-ruby-default-implementation "pry"))

;;(setq ruby-indent-tabs-mode t)
(use-package ruby-end)
;;Electric pair mode
;;https://github.com/daedreth/UncleDavesEmacs
(setq electric-pair-pairs '((?\` . ?\`)))
(electric-pair-mode t)

(add-hook 'ruby-mode-hook
					(lambda ()
						(ruby-end-mode 1)
						(diminish 'ruby-end-mode)))
(add-hook 'ruby-mode-hook 'hs-minor-mode)

;; convert json to hash
(use-package ruby-json-to-hash)

;;Move backups to temp directory.  Who needs that crap, anyway?
;;Note that the temp directory is set to keep them for 30 days,
;;so it's not entirely stupid.
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;;Enable universal copy-paste(to system clipboard).
(setq select-enable-clipboard t)
(setq interprogram-paste-function 'x-selection-value)

;;flips two windows from horizontal split to vertical split
(defun rotate-windows ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

;;flips what's in two buffers with each other.
(defun flip-windows ()
  "Flip your windows"
  (interactive)
  (cond ((not (> (count-windows)1))
         (message "You can't rotate a single window!"))
        (t
         (setq i 1)
         (setq numWindows (count-windows))
         (while  (< i numWindows)
           (let* (
                  (w1 (elt (window-list) i))
                  (w2 (elt (window-list) (+ (% i numWindows) 1)))

                  (b1 (window-buffer w1))
                  (b2 (window-buffer w2))

                  (s1 (window-start w1))
                  (s2 (window-start w2))
                  )
             (set-window-buffer w1  b2)
             (set-window-buffer w2 b1)
             (set-window-start w1 s2)
             (set-window-start w2 s1)
             (setq i (1+ i)))))))

;;Sticky-buffer-mode
;; https://gist.github.com/ShingoFukuyama/8797743
;; http://lists.gnu.org/archive/html/help-gnu-emacs/2007-05/msg00975.html
(defvar sticky-buffer-previous-header-line-format)
(define-minor-mode sticky-buffer-mode
  "Make the current window always display this buffer."
  nil " sticky" nil
  (if sticky-buffer-mode
      (progn
        (set (make-local-variable 'sticky-buffer-previous-header-line-format)
             header-line-format)
        (set-window-dedicated-p (selected-window) sticky-buffer-mode))
    (set-window-dedicated-p (selected-window) sticky-buffer-mode)
    (setq header-line-format sticky-buffer-previous-header-line-format)))

(use-package go-mode
  :hook (before-save . gofmt-before-save))
(put 'downcase-region 'disabled nil)

(use-package stripe-buffer
  :defer 0
  :config
  (add-hook 'dired-mode-hook 'turn-on-stripe-buffer-mode)
  (add-hook 'org-mode-hook 'turn-on-stripe-table-mode))

;;https://superuser.com/questions/576447/enable-hideshow-for-more-modes-e-g-ruby
(use-package hideshow
  :diminish hs-minor-mode
  :hook (prog-mode . hs-minor-mode)
  :config
  (add-to-list 'hs-special-modes-alist
               `(ruby-mode
                 ,(rx (or "def" "class" "module" "do" "{" "["))
                 ,(rx (or "}" "]" "end"))
                 ,(rx (or "#" "=begin"))
                 ruby-forward-sexp nil))
  (global-set-key (kbd "C-c h") 'hs-toggle-hiding))

;;ledger mode for accounting.
;;Accounts are stored in Documents/finances/ledger/ledger-xxxx.dat
(use-package ledger-mode
  :mode ("\\.dat\\'"
         "\\.ledger\\'")
  :config
  (setq ledger-reports
        `(("bal" ,(concat "%(binary) [[ledger-mode-flags]] -f " my-ledger-file " bal not Equity --current"))
          ("reg" ,(concat "%(binary) [[ledger-mode-flags]] -f " my-ledger-file " reg not Equity -S -date --current"))
          ("balr" ,(concat "%(binary) [[ledger-mode-flags]] -f " my-ledger-file " bal not Equity --real --current"))
          ("budget" ,(concat "%(binary) [[ledger-mode-flags]] -f " my-ledger-file " bal Budget --current"))
          ("payee" "%(binary) -f %(my-ledger-file) reg @%(payee)")
          ("account" ,(concat "%(binary) [[ledger-mode-flags]] -f " my-ledger-file " reg %(account)"))))
  (setq ledger-reconcile-sort-key "(date)")

  :custom (ledger-clear-whole-transactions t))

(use-package restclient
  :mode ("\\.http\\'" . restclient-mode))

(use-package csv
  :mode ("\\.csv\\'" . csv-mode))

;;from http://pragmaticemacs.com/emacs/dynamically-filter-directory-listing-with-dired-narrow/
(use-package dired-narrow
  :demand
  :bind
  (:map dired-mode-map ("/" . dired-narrow-regexp)))

(use-package dockerfile-mode)

(use-package arm-mode
  :load-path "elpa/arm-mode/"
  :mode ("\\.s\\'"))

;;Org-roam, for zettelkasten
(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :diminish org-roam
  :hook
  (after-init . org-roam-setup)
  :custom
  (org-roam-directory "~/org notes and todos/roam")
  :config
  (setq org-roam-capture-templates
        '(("d" "default" plain "%?"
           :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n#+filetags: ${filetags}")
           :unnarrowed t)))
  :bind
  (("C-c n f" . org-roam-node-find)
   ("C-c n j" . org-roam-jump-to-index)
   ("C-c n b" . org-roam-buffer-toggle)
   ("C-c n g" . org-roam-graph)
   ("C-c n i" . org-roam-capture)))

;;dump-jump, go to definition.  Better than tags
(use-package dumb-jump
  :defer 0
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

;;convert buffer text to html
(use-package htmlize
  :config
  (setq htmlize-output-type 'inline-css))

(use-package css-mode
  :ensure nil
  :config
  (setq css-indent-offset 2))

(use-package expand-region
  :bind
  ("C-=" . er/expand-region))

(use-package slim-mode)

;;cleans trailing whitespace, etc
(use-package ethan-wspace
  :diminish
  :defer 0
  :init
  (setq mode-require-final-newline nil)
  :config
  (global-ethan-wspace-mode 1))

;;typescript
(use-package typescript-mode
  :hook
  (typescript-mode . eglot-ensure)
  (typescript-mode . origami-mode)
  :mode "\\.tsx?$")


;;electric-operator mode
;;automatically adds spaces around operators
(use-package electric-operator
  :demand t
  :config
  (setq electric-operator-c-pointer-type-style 'type)
  (electric-operator-add-rules-for-mode 'ruby-mode
                                        (cons "?" nil)
                                        (cons ":" nil)
                                        (cons "&" nil)
                                        (cons "|" nil)
                                        (cons "=>" " => ")
                                        (cons "<<" " << ")
                                        (cons "||=" " ||= ")
                                        (cons "**" " **")
                                        (cons "%i" " %i["))
  (electric-operator-add-rules-for-mode 'csharp-mode
                                        (cons "(;)" "();"))
  :hook
  (ruby-mode . electric-operator-mode)
  (c-mode . electric-operator-mode)
  (csharp-mode . electric-operator-mode)
  (python-mode . electric-operator-mode))

;;crux-mode
;;a bunch of useful text editing commands
(use-package crux
  :bind
  (("C-c e" . crux-eval-and-replace)
   ("C-c I" . crux-find-user-init-file)
   ("M-k" . crux-kill-whole-line)))

;;vterm
;;a better in-emacs terminal
(use-package vterm
  :config
  (setq vterm-copy-exclude-prompt t)
  (setq vterm-min-window-width 40))
(use-package multi-vterm)

(use-package scratch
  :bind ("C-c s" . #'scratch))

;;rust
(use-package rust-mode
  :hook
  (rust-mode . eglot-ensure)
  :config
  (setq rust-format-on-save nil))

;;csharp + eglot
(use-package csharp-mode
  :hook
  (csharp-mode . eglot-ensure)
  (csharp-mode . origami-mode)
  :bind
  ("C-c h" . origami-toggle-node))

(use-package origami
  :defer 0
  :config
  (add-to-list 'origami-parser-alist `(csharp-mode . ,(origami-markers-parser "#region" "#endregion"))))

(use-package eglot)

(use-package dap-mode
  :config
  (require 'dap-netcore)
  (dap-mode 1)
  (dap-auto-configure-mode)
  (dap-ui-mode 1)
  :after lsp-mode)

(use-package orderless
  :defer 0
  :config
  (setq completion-styles '(flex orderless basic))
  (setq read-file-name-completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t))

(use-package vertico
  :defer 0
  :bind
  (:map vertico-map
        ("TAB" . vertico-directory-enter)
        ("\\" . vertico-directory-up))
  :init (vertico-mode))

(use-package consult
  :defer 0)

(use-package corfu
  :defer 0
  :config
  (setq corfu-auto t)
  (setq corfu-auto-prefix 3)
  (setq corfu-quit-no-match t)
  :init
  (global-corfu-mode))

(use-package kotlin-mode
  :hook
  (kotlin-mode . eglot-ensure))
